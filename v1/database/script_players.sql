CREATE DATABASE games; 
USE games;
CREATE TABLE players(
	id INT AUTO_INCREMENT NOT NULL, 
    nom VARCHAR(255), 
    prenom VARCHAR(255), 
    login VARCHAR(255), 
    mdp VARCHAR(255), 
    PRIMARY KEY (id)
);

insert into players (id, nom, prenom, login, mdp) values (1, 'owain', 'rainbow', 'owain', 'azerty');
insert into players (id, nom, prenom, login, mdp) values (2, 'Whittlesee', 'Ruggiero', 'rwhittlesee1', 'dYR2yUu');
insert into players (id, nom, prenom, login, mdp) values (3, 'Heiton', 'Ward', 'wheiton2', 'kCBAH9Fjmo');
insert into players (id, nom, prenom, login, mdp) values (4, 'Marns', 'Caesar', 'cmarns3', 'q5sv8b');
insert into players (id, nom, prenom, login, mdp) values (5, 'Cale', 'Kimbell', 'kcale4', 'sNDe4NxH');
insert into players (id, nom, prenom, login, mdp) values (6, 'Gayne', 'Catherina', 'cgayne5', 'mDArCy');
insert into players (id, nom, prenom, login, mdp) values (7, 'Bywaters', 'Kermie', 'kbywaters6', 'v9h9R1');
insert into players (id, nom, prenom, login, mdp) values (8, 'Haldin', 'Lyn', 'lhaldin7', 'VZ3npUb');
insert into players (id, nom, prenom, login, mdp) values (9, 'Hutsby', 'Laural', 'lhutsby8', 'c3yKzKclgk');
insert into players (id, nom, prenom, login, mdp) values (10, 'Diwell', 'Colly', 'cdiwell9', 'bIjE4ud0p');
insert into players (id, nom, prenom, login, mdp) values (11, 'Roles', 'Hurleigh', 'hrolesa', 'U1mfbE');
insert into players (id, nom, prenom, login, mdp) values (12, 'Couch', 'Ashlen', 'acouchb', 'MvG06NF');
insert into players (id, nom, prenom, login, mdp) values (13, 'Crossfeld', 'Andrej', 'acrossfeldc', 'JwErJeBn105k');
insert into players (id, nom, prenom, login, mdp) values (14, 'Hrinishin', 'Berry', 'bhrinishind', 'vuTHncLMPTm');
insert into players (id, nom, prenom, login, mdp) values (15, 'Moehle', 'Vanna', 'vmoehlee', 'NTnY3fT4TxX1');
insert into players (id, nom, prenom, login, mdp) values (16, 'Flaubert', 'Luci', 'lflaubertf', '5T0xk0M');
insert into players (id, nom, prenom, login, mdp) values (17, 'Billings', 'Vinni', 'vbillingsg', 'ERZPlL');
insert into players (id, nom, prenom, login, mdp) values (18, 'Rickford', 'Roscoe', 'rrickfordh', 'AJklUbkzZ');
insert into players (id, nom, prenom, login, mdp) values (19, 'Greschik', 'Ulla', 'ugreschiki', '3k6IexqM4');
insert into players (id, nom, prenom, login, mdp) values (20, 'Medhurst', 'Sherilyn', 'smedhurstj', 'Ht5RtoSFUaD');