<?php

   include("config.php");
   session_start();
   
   if($_SERVER["REQUEST_METHOD"] == "POST") {
      // username and password sent from form 
      
      $myusername = mysqli_real_escape_string($db,$_POST['login']);
      $mypassword = mysqli_real_escape_string($db,$_POST['password']); 
      
      $sql = "SELECT id FROM players WHERE login = '$myusername' and mdp = '$mypassword'";
      $result = mysqli_query($db,$sql);
      $row = mysqli_fetch_array($result,MYSQLI_ASSOC);
      # $active = $row['active'];
      
      $count = mysqli_num_rows($result);
      
      // If result matched $myusername and $mypassword, table row must be 1 row
		
      if($count == 1) {
         #session_register("myusername");
         $_SESSION['login_user'] = $myusername;
         
         header("location: profil.php");
      }else {
         $error = "Your Login Name or Password is invalid";
         header("location: subscribe.php");
      }
   }
?>


<?php
require('header.php');
?>
    <div class="container">
        <div id="loginForm" class="container mt-5 col-md-6 offset-md-3">
            <h1>Formulaire de connexion</h1><br>
            <?php if($error):?>
                <div class="alert alert-danger" role="alert">
                    <?= $error ?>
                </div>
            <?php endif ?>

            <form class="shadow-lg p-3 mb-5  mt-4 bg-body rounded" action="" method="POST" required>
                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">Login</label>
                    <input type="text" class="form-control" id="login" placeholder="saisie du login" name="login"
                        required>
                </div>
                <div class="mb-3">
                    <label for="password" class="form-label">Password</label>
                    <input type="password" class="form-control" id="password" placeholder="saisie du mot de passe"
                        name="password">
                </div>
                <div>
                    <button type="submit" class="btn btn-primary btn-lg btn-block">Se connecter</button>
                    <!-- <button type="button" class="btn btn-warning btn-lg"
                        onclick="location.href='inscription'">S'inscrire</button> -->
                </div>

                <!-- <div class="text-center">
                    <button type="submit" class="btn btn-primary btn-lg">Connexion</button>
                </div> -->
                <!-- <div class='mt-4'>
                    <p class="text-danger text-center">${msg}
                    <p>
                </div>
            -->
            </form>
        </div>
    </div>
</body>
</html>