<?php
   include("config.php");

   if($_SERVER["REQUEST_METHOD"] == "POST") {
    // username and password sent from form 
    
    $nom = mysqli_real_escape_string($db,$_POST['nom']);
    $prenom = mysqli_real_escape_string($db,$_POST['prenom']); 
    $login = mysqli_real_escape_string($db,$_POST['login']); 
    $mdp = mysqli_real_escape_string($db,$_POST['password']); 

    $sql = "INSERT INTO players ".
               "(nom, prenom, login, mdp) "."VALUES ".
               "('$nom','$prenom','$login', '$mdp')";
    
    if (mysqli_query($db, $sql)) {
        echo "New record created successfully";
      } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($db);
      }
      mysqli_close($db);

      header("location: login.php");

   }
?>

<?php
require('header.php');
?>
    <div class="container mt-5 col-md-6 offset-md-3">
        <h1>Formulaire d'inscription</h1>
        <form class="shadow-lg p-3 mb-5 bg-body rounded" action='' method="POST">
            <div class="mb-3">
                <label for="frmNom" class="form-label">Nom</label>
                <input type="text" class="form-control" id="frmNom" placeholder="Votre nom" required name="nom">
            </div>
            <div class="mb-3">
                <label for="frmPrenom" class="form-label">Prénom</label>
                <input type="text" class="form-control" id="frmPrenom" placeholder="Votre prénom" required
                    name="prenom">
            </div>
            <div class="mb-3">
                <label for="frmLogin" class="form-label">Login</label>
                <input type="text" class="form-control" id="frmLogin" placeholder="Votre login" required name="login">
            </div>
            <div class="mb-3">
                <label for="frmPassword" class="form-label">Password</label>
                <input type="text" class="form-control" id="frmPassword" placeholder="Votre mot de passe" required
                    name="password">
            </div>
            <button type="submit" class="btn btn-primary">S'inscrire</button>
        </form>
    </div>
</body>

</html>