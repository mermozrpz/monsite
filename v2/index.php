<?php
require('header.php');
?>
    <div class="container">
        <h1 class="mt-4">Home page</h1>
        <div class="row">
            <img src="https://img.freepik.com/vecteurs-libre/touristes-sacs-enregistrement-hotel_74855-5297.jpg?t=st=1652880735~exp=1652881335~hmac=965cdf6f15ea083c74b65b69297c7de98eaf2ecc462d5de03f0fb6421fe8ec70&w=1380"
                class="img-fluid" alt="home">
        </div>

        <div class="row text-center">
            <p>Vestibulum hendrerit sed ante nec pharetra. Aenean hendrerit vestibulum arcu, quis rutrum ligula pulvinar
                consequat. Suspendisse dictum leo eu iaculis pharetra. Proin finibus nunc eget tristique ultrices.
                Maecenas auctor et sem ut pulvinar. Aliquam quam leo, facilisis sed aliquet sit amet, fermentum id
                libero. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed
                sollicitudin eros vel sem interdum tristique. Fusce pretium ut nisi in pretium. Integer elit dolor,
                pulvinar quis risus a, fringilla viverra tellus. Morbi sapien risus, interdum a nisl ac, sollicitudin
                ultrices odio. Integer tempor risus sit amet nunc semper, quis dictum ligula vehicula. Sed efficitur
                suscipit condimentum. Sed bibendum posuere tempor. Pellentesque habitant morbi tristique senectus et
                netus et malesuada fames ac turpis egestas.</p>
        </div>
    </div>
</body>

</html>